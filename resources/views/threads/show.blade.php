@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href="#">{{$thread->creator->name}}</a> posted: <strong>{{$thread->title}}</strong>
                    </div>

                    <div class="panel-body">
                        {{$thread->body}}
                        <hr>
                    </div>
                </div>

                @foreach($replies as $reply)
                    @include('threads.partials.reply')
                @endforeach

                {{$replies->links()}}

                @if(auth()->check())
                    <form action="{{ $thread->path() . '/replies' }}" method="post">
                        {{csrf_field()}}
                        <div class="form-group">
                    <textarea name="body" id="body" cols="30" rows="5" placeholder="Have something to say"
                              class="form-control"></textarea>
                        </div>

                        <button class="btn btn-primary">POST</button>
                    </form>
                @else
                    <p class="text-center">Please <a href="{{route('login')}}">Sign In</a> to participate in the forum
                    </p>
                @endif
            </div>
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <p>This thread was published {{$thread->created_at->diffForHumans() }} by
                            <a href="#">{{$thread->creator->name}}</a>, and currently
                            has {{$thread->replies_count}} {{str_plural('comment', $thread->replies_count)}}.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
