<?php

namespace App\Filters;

use App\User;

class ThreadFilters extends Filters
{
    protected $filters = ['by', 'popular'];

    /**
     * @param $username
     * @return mixed
     */
    protected function by($username)
    {
        $user = User::where('name', $username)->firstOrFail();

        return $this->builder->where('user_id', $user->id);
    }

    /*
     * filter query by most popular replies count
     */
    protected function popular()
    {
        $this->builder->getQuery()->orders = []; // clear existing orders query

        return $this->builder->orderBy('replies_count', 'desc');
    }
}
